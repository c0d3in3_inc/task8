package com.c0d3in3.task7

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.InputType
import android.widget.EditText
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

@SuppressLint("AppCompatCustomView", "ViewConstructor")
class CustomEditText(context: Context, model: AuthModel) : EditText(context) {
    var required = false
    var fieldId = -1

    init {
        this.setHintTextColor(Color.GRAY)
        this.setTextColor(Color.BLACK)
        this.hint = model.hint
        this.fieldId = model.fieldId
        this.required = model.required
        when(model.keyboard){
            "email" -> this.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            "number" -> this.inputType = InputType.TYPE_CLASS_NUMBER
            else -> this.inputType = InputType.TYPE_CLASS_TEXT
        }
        val iconSize = convertDpToPixel(25.0)
        Glide.with(this).asDrawable().load(model.icon).into(object:
            CustomTarget<Drawable>(iconSize, iconSize) {
            override fun onLoadCleared(placeholder: Drawable?) {
            }

            override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                this@CustomEditText.setCompoundDrawablesWithIntrinsicBounds(null, null, resource, null)
            }
        })
        if(model.fieldType == "chooser"){
            this.isFocusable = false
            this.isClickable = true
            this.setOnClickListener {
                Toast.makeText(App.getInstance().applicationContext, "Select your ${model.hint}", Toast.LENGTH_SHORT).show()
            }
        }

    }
}