package com.c0d3in3.task7

import android.app.Application

class App : Application(){
    companion object{
        private lateinit var instance: Application
        fun getInstance() = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}