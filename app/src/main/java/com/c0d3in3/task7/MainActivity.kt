package com.c0d3in3.task7


import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray


class MainActivity : AppCompatActivity() {

    companion object{
        const val REGISTER_BUTTON = 22
    }

    private val fieldObjects = arrayListOf<Any>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

    }

    private fun init(){
        val json = JSONArray(customJson)
        var cardView : CardView
        var childLayout : LinearLayout
        (0 until json.length()).forEach {
            val prod = json.getJSONArray(it)
            cardView = createCardView()
            childLayout = addChildLayout(cardView)
            (0 until prod.length()).forEach { it2->
                val model = Gson().fromJson(prod.getJSONObject(it2).toString(), AuthModel::class.java)
                createField(model, childLayout)
            }
            authLayout.addView(cardView)
        }
        addRegisterButton()
    }

    private fun createButton(model : AuthModel, customLayout: LinearLayout){
//        val button = Button(this)
//        val layoutParams = FrameLayout.LayoutParams(
//            FrameLayout.LayoutParams.MATCH_PARENT,
//            FrameLayout.LayoutParams.WRAP_CONTENT
//        )
//        button.layoutParams = layoutParams
//        button.text = model.hint
//        button.setBackgroundResource(R.drawable.chooser_background)
//        button.setTextColor(Color.BLACK)
//        customLayout.addView(button)
//        fieldObjects.add(button)
        val editText = CustomEditText(this, model)
        val layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        val dp = convertDpToPixel(5.0)
        layoutParams.setMargins(dp, dp, dp , dp)
        editText.layoutParams = layoutParams
        customLayout.addView(editText)
        fieldObjects.add(editText)
    }

    private fun createTextField(model: AuthModel, customLayout: LinearLayout){
        val editText = CustomEditText(this, model)
        val layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        val dp = convertDpToPixel(5.0)
        layoutParams.setMargins(dp, dp, dp , dp)
        editText.layoutParams = layoutParams
        customLayout.addView(editText)
        fieldObjects.add(editText)
    }

    private fun createField(model : AuthModel, customLayout: LinearLayout){
        if(!model.isActive) return
        when(model.fieldType) {
            "chooser" -> createButton(model, customLayout)
            else -> createTextField(model, customLayout)
        }
    }

    private fun createCardView(): CardView {
        val cardView = CardView(this)
        val layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        val dp = convertDpToPixel(10.0)
        layoutParams.setMargins(dp, dp, dp , dp)
        cardView.layoutParams = layoutParams
        cardView.radius = 15F
        cardView.setCardBackgroundColor(Color.WHITE)
        return cardView
    }

    private fun addChildLayout(cardView: CardView) : LinearLayout{
        val linearLayout = LinearLayout(this)
        val linearLayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        linearLayout.layoutParams = linearLayoutParams
        linearLayout.orientation = LinearLayout.VERTICAL
        cardView.addView(linearLayout)
        return linearLayout
    }

    private fun addRegisterButton(){
        val button = Button(this)
        val layoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        val paddingDp = convertDpToPixel(15.0)
        layoutParams.setMargins(paddingDp, paddingDp, paddingDp, paddingDp)
        button.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
        button.layoutParams = layoutParams
        button.text = resources.getString(R.string.register)
        button.setBackgroundResource(R.drawable.register_button_background)
        button.id = REGISTER_BUTTON
        button.setOnClickListener {
            register()
        }
        authLayout.addView(button)
    }

    private fun register(){
        val params = mutableMapOf<String, String>()
        var success = true
        fieldObjects.forEach {
            if(it is CustomEditText)
            {
                if(it.required && it.text.toString().isEmpty()) {
                    Toast.makeText(this, "${it.hint} is empty!", Toast.LENGTH_SHORT).show()
                    success = false
                    return
                }
                params[it.fieldId.toString()] = it.text.toString()
            }
        }
        if(success) println(params)
    }
}
