package com.c0d3in3.task7

import android.content.res.Resources
import android.util.TypedValue

fun convertDpToPixel(dp: Double): Int{
    val r: Resources = App.getInstance().applicationContext.resources
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp.toFloat(),
        r.displayMetrics
    ).toInt()
}

const val customJson = "[\n" +
        "   [\n" +
        "      {\n" +
        "         \"field_id\":1,\n" +
        "         \"hint\":\"UserName\",\n" +
        "         \"field_type\":\"input\",\n" +
        "         \"keyboard\":\"text\",\n" +
        "         \"required\":false,\n" +
        "         \"is_actove\":true,\n" +
        "         \"icon\":\"https://img.icons8.com/material/4ac144/256/user-male.png\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"field_id\":2,\n" +
        "         \"hint\":\"Email\",\n" +
        "         \"field_type\":\"input\",\n" +
        "         \"required\":true,\n" +
        "         \"keyboard\":\"text\",\n" +
        "         \"is_actove\":true,\n" +
        "         \"icon\":\"https://jemala.png\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"field_id\":3,\n" +
        "         \"hint\":\"phone\",\n" +
        "         \"field_type\":\"input\",\n" +
        "         \"required\":true,\n" +
        "         \"keyboard\":\"number\",\n" +
        "         \"is_actove\":true,\n" +
        "         \"icon\":\"https://jemala.png\"\n" +
        "      }\n" +
        "   ],\n" +
        "   [\n" +
        "      {\n" +
        "         \"field_id\":4,\n" +
        "         \"hint\":\"Full Name\",\n" +
        "         \"Field_type\":\"input\",\n" +
        "         \"keyboard\":\"text\",\n" +
        "         \"required\":true,\n" +
        "         \"is_actove\":true,\n" +
        "         \"icon\":\"https://jemala.png\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"field_id\":89,\n" +
        "         \"hint\":\"Birthday\",\n" +
        "         \"field_type\":\"chooser\",\n" +
        "         \"required\":false,\n" +
        "         \"is_actove\":true,\n" +
        "         \"icon\":\"https://jemala.png\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"field_id\":898,\n" +
        "         \"hint\":\"Gender\",\n" +
        "         \"field_type\":\"chooser\",\n" +
        "         \"required\":\"false\",\n" +
        "         \"is_actove\":true,\n" +
        "         \"icon\":\"https://jemala.png\"\n" +
        "      }\n" +
        "   ]\n" +
        "]"