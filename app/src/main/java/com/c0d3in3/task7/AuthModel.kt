package com.c0d3in3.task7

import com.google.gson.annotations.SerializedName

class AuthModel {
    @SerializedName("field_id")
    var fieldId = -1
    var hint = ""
    @SerializedName("field_type")
    var fieldType = ""
    var keyboard = ""
    var required = false
    @SerializedName("is_actove")
    var isActive = false
    var icon = ""
}